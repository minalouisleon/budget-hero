import {
  IonBackButton,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { useState } from "react";
import dayjs from "dayjs";
import { Line } from "react-chartjs-2";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);
const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Month Distribution Chart',
    },
  },
};
const MonthDistribution = () => {
  const params = useParams(null);
  console.log(params);
  const monthDistributionData = useSelector(
    (state) => state.tempReducer.monthDistributionData
  );
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="ios">
          <IonBackButton slot="start" />
          <IonTitle>{params.name}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <Line
          data={{
            labels: monthDistributionData.labels,
            datasets: monthDistributionData.datasets,
          }}
          options={options}
        />
      </IonContent>
    </IonPage>
  );
};

export default MonthDistribution;
