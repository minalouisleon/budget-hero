import {
  IonBackButton,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import dayjs from "dayjs";
import { useParams, useHistory } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { actionSetMonthDistributionData } from "../../redux/tempReducer";
const BudgetChart = () => {
  const params = useParams(null);
  const history = useHistory(null);
  const dispatch = useDispatch(null);
  const budgetData = useSelector(
    (state) => state.budgetReducer.budgetsData[params.index].data
  );
  const handleMonthDistribution = async () => {
    const daysInMonth = dayjs().daysInMonth();
    const today = parseInt(dayjs().format("DD"));
    const numberOfDays = daysInMonth - today + 1;
    const perDayBudget = params.totalBudget / numberOfDays;
    let labels = [];
    let datasets = [
      {
        type: "line",
        label: "Income",
        borderColor: "rgb(56, 128, 255)",
        backgroundColor: "rgba(56, 128, 255, 0.5)",
        data: [],
      },
      {
        type: "line",
        label: "Expens",
        borderColor: "rgb(255, 99, 132)",
        backgroundColor: "rgba(255, 99, 132, 0.5)",
        data: [],
      },
      {
        type: "line",
        label: "Expected Expens",
        borderColor: "rgb(45, 211, 111)",
        backgroundColor: "rgba(45, 211, 111, 0.5)",
        data: [],
      },
    ];
    for (let i = 1; i <= daysInMonth; i++) {
      labels.push(`${i}`);
      let incomeAmount = 0.0;
      let expensAmount = 0.0;
      await budgetData.forEach((element) => {
        if (parseInt(element.date.split("-")[2]) === i) {
          if (element.type === "Income") {
            incomeAmount = incomeAmount + parseFloat(element.amount);
          } else if (element.type === "Expens") {
            expensAmount = expensAmount + parseFloat(element.amount);
          }
        }
        // console.log(datasets)
      });
      datasets[0].data.push(incomeAmount);
      datasets[1].data.push(expensAmount);
      if (i < today) {
        datasets[2].data.push(0);
      } else {
        datasets[2].data.push(perDayBudget);
      }
    }
    console.log(datasets);
    dispatch(
      actionSetMonthDistributionData({
        datasets: datasets,
        labels: labels,
      })
    );
    history.push(`/MonthDistribution/${params.name}/${params.index}`);
  };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="ios">
          <IonBackButton slot="start" />
          <IonTitle>{params.name}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList mode="ios">
          <IonItem onClick={() => handleMonthDistribution()}>
            <IonLabel>Month Distribution</IonLabel>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default BudgetChart;
