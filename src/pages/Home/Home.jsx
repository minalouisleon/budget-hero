import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import LoginLogoutBtn from "../../components/LoginLogoutBtn/LoginLogoutBtn";
import { useHistory } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { actionDeleteBudget } from "../../redux/budgetReducer";
const Home = () => {
  const history = useHistory(null);
  const dispatch = useDispatch(null);
  const budgets = useSelector((state) => state.budgetReducer.budgets);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="ios">
          <IonButtons slot="start">
            <IonButton onClick={() => history.push("/AddBudget")}>
              Add Budget
            </IonButton>
          </IonButtons>
          <IonTitle>Budget Hero</IonTitle>
          <IonButtons slot="end">
            <LoginLogoutBtn />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList mode="ios">
          {budgets &&
            budgets.map((budget, index) => {
              return (
                <IonItem key={budget}>
                  <IonLabel>{budget}</IonLabel>
                  <IonButtons size='small'>
                    <IonButton
                      size="small"
                      fill="outline"
                      onClick={() =>
                        history.push(`/BudgetPage/${budget}/${index}`)
                      }
                    >
                      Open
                    </IonButton>
                    <IonButton
                      size="small"
                      fill="outline"
                      color="danger"
                      onClick={() => dispatch(actionDeleteBudget(budget))}
                    >
                      Delete
                    </IonButton>
                  </IonButtons>
                </IonItem>
              );
            })}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Home;
