import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { useState } from "react";
import { useParams, useHistory } from "react-router";
import { barChartOutline } from "ionicons/icons";
import { useSelector } from "react-redux";
import AddAmount from "../../components/AddAmount/AddAmount";
const BudgetPage = () => {
  const params = useParams(null);
  const history = useHistory(null);
  const budgetData = useSelector(
    (state) => state.budgetReducer.budgetsData[params.index]
  );
  const [isAddAmountModalOpen, setIsAddAmountModalOpen] = useState(false);
  const [AddAmountType, setAddAmountType] = useState("");
  return (
    <IonPage>
      <AddAmount
        index={params.index}
        isAddAmountModalOpen={isAddAmountModalOpen}
        setIsAddAmountModalOpen={setIsAddAmountModalOpen}
        AddAmountType={AddAmountType}
        setAddAmountType={setAddAmountType}
      />
      <IonHeader>
        <IonToolbar mode="ios">
          <IonBackButton slot="start" />
          <IonTitle>{params.name}</IonTitle>
          <IonButtons slot="end">
            <IonButton
              fill="outline"
              onClick={() =>
                history.push(`/BudgetChart/${params.name}/${params.index}/${budgetData.total}`)
              }
            >
              <IonIcon icon={barChartOutline} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList mode="ios">
          <IonItem>
            <IonLabel>Total Budget : {budgetData.total}</IonLabel>
            <IonButtons slot="end">
              <IonButton
                fill="outline"
                onClick={() =>
                  history.push(`/BudgetDetails/${params.name}/${params.index}`)
                }
              >
                Open
              </IonButton>
              <IonButton
                size="small"
                fill="outline"
                color="success"
                onClick={() => {
                  setAddAmountType("Income");
                  setIsAddAmountModalOpen(true);
                }}
              >
                Add Income
              </IonButton>
              <IonButton
                size="small"
                fill="outline"
                color="danger"
                onClick={() => {
                  setAddAmountType("Expens");
                  setIsAddAmountModalOpen(true);
                }}
              >
                Add Expens
              </IonButton>
            </IonButtons>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default BudgetPage;
