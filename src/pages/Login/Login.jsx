import React from "react";
import "./Login.css";
import { auth } from "../../firebase/firebaseConfig";
import { signInWithEmailAndPassword } from "firebase/auth";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { message } from "antd";
import { actionLoginUser } from "../../redux/userReducer";
const Login = () => {
  const history = useHistory(null);
  const dispatch = useDispatch(null);
  const handleLogin = (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(
      auth,
      `${e.target[0].value}@budget.hero`,
      e.target[1].value
    )
      .then((userCredential) => {
        dispatch(
          actionLoginUser({
            email: userCredential.user.email,
            uid: userCredential.user.uid,
          })
        );
        history.replace("/");
      })
      .catch((error) => {
        if (error.code === "auth/user-not-found") {
          message.error("Wrong Username");
        } else if (error.code === "auth/wrong-password") {
          message.error("Wrong Password");
        } else if (error.code === "auth/network-request-failed") {
          message.error("No Internet");
        }
      });
  };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="ios">
          <IonBackButton slot="start" />
          <IonTitle>Login</IonTitle>
        </IonToolbar>
        <IonContent fullscreen>
          <div className="loginPageMainContainerStyle">
            <IonCard>
              <form onSubmit={handleLogin}>
                <IonList>
                  <IonItem>
                    <IonLabel>Username :</IonLabel>
                    <IonInput type="text" required />
                  </IonItem>
                  <IonItem>
                    <IonLabel>Password :</IonLabel>
                    <IonInput type="password" required />
                  </IonItem>
                  <div className="tc">
                    <IonButton color="success" type="submit">
                      Login
                    </IonButton>
                  </div>
                </IonList>
              </form>
            </IonCard>
          </div>
        </IonContent>
      </IonHeader>
    </IonPage>
  );
};

export default Login;
