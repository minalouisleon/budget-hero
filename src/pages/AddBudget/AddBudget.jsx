import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import React from 'react'
import LoginLogoutBtn from '../../components/LoginLogoutBtn/LoginLogoutBtn'
import { useDispatch,useSelector } from 'react-redux'
import {message} from 'antd';
import { actionAddBudget } from '../../redux/budgetReducer';
import { useHistory } from 'react-router';
const AddBudget = () => {
    const dispatch = useDispatch(null);
    const history = useHistory(null);
    const budgets = useSelector(state => state.budgetReducer.budgets);
    const handleAddBudget = (e) => {
        e.preventDefault();
        if(budgets.includes(e.target[0].value)){
            message.error('Budget Name Exist !')
        }else{
            dispatch(actionAddBudget({
                name : e.target[0].value,
                amount : e.target[1].value
            }));
            history.replace('/');
        }
    }
  return (
    <IonPage>
        <IonHeader>
            <IonToolbar mode='ios'>
                <IonBackButton slot='start' />
                <IonTitle>Add Budget</IonTitle>
                <IonButtons slot='end'>
                    <LoginLogoutBtn />
                </IonButtons>
            </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
            <form onSubmit={handleAddBudget}>
            <IonList>
                <IonItem>
                    <IonLabel>Name :</IonLabel>
                    <IonInput required type='text' />
                </IonItem>
                <IonItem>
                    <IonLabel>Amount :</IonLabel>
                    <IonInput required type='number' step='0.01' />
                </IonItem>
                <div className='tc'>
                    <IonButton color='success' type='submit'>Add</IonButton>
                </div>
            </IonList>
            </form>
        </IonContent>
    </IonPage>
  )
}

export default AddBudget
