import { IonBackButton, IonContent, IonHeader, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router'
const BudgetDetails = () => {
    const params = useParams(null);
    const budgetData = useSelector(state => state.budgetReducer.budgetsData[params.index]);
  return (
    <IonPage>
        <IonHeader>
            <IonToolbar mode='ios'>
                <IonBackButton slot='start'/>
                <IonTitle>
                    Total : {budgetData.total}
                </IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
            <IonList>
                {budgetData && budgetData.data.map((item,index) => {
                    return(
                        <IonItem lines='full' key={`amount-${index}`}>
                            <IonLabel color={item.type === 'Income' ? 'success' : 'danger'} slot='start'>
                                {item.label}
                            </IonLabel>
                            <IonLabel color={item.type === 'Income' ? 'success' : 'danger'} slot='end'>
                                amount : {item.amount}
                            </IonLabel>
                        </IonItem>
                    )
                })}
            </IonList>
        </IonContent>
    </IonPage>
  )
}

export default BudgetDetails
