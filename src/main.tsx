import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import 'tachyons'
// @ts-ignore
import {store} from './redux/store';
import {persistStore} from 'redux-persist';
import {PersistGate} from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
const container = document.getElementById('root');

const persistor = persistStore(store);

const root = createRoot(container!);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </React.StrictMode>
);