import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    uid : '',
    email : '',
    isLoggedIn : false,
    settings : {}
}

const userReducer  = createSlice({
    name : 'userReducer',
    initialState,
    reducers : {
        actionLoginUser : (state,action) => {
            /**
             * args : {
             *      uid : string,
             *      email : string
             * }
             */
            state.uid = action.payload.uid;
            state.email = action.payload.email,
            state.isLoggedIn = true
        },
        actionLogoutUser : (state,action) => {
            state.uid = '';
            state.email = '';
            state.isLoggedIn = false
        }
    }
})

export const {actionLoginUser,actionLogoutUser} = userReducer.actions;
export default userReducer.reducer;