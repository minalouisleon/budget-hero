import { combineReducers } from "redux";
import storage from 'redux-persist/lib/storage';
import { persistReducer } from "redux-persist";
import { configureStore} from '@reduxjs/toolkit';
import userReducer from "./userReducer";
import budgetReducer from "./budgetReducer";
import tempReducer from "./tempReducer";
const reducers = combineReducers({
    userReducer,
    budgetReducer,
    tempReducer
})

const persistConfig = {
    key : 'root',
    storage,
    whitelist : ['userReducer','budgetReducer']
}

const persistedReducer = persistReducer(persistConfig,reducers);

export const store = configureStore({
    reducer : persistedReducer,
    devTools : true
})