import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  budgets: [],
  budgetsData: [],
};

const budgetReducer = createSlice({
  name: "budgetReducer",
  initialState,
  reducers: {
    actionAddBudget: (state, action) => {
      /**
       * args : {
       *      name : string
       *      amount : number
       * }
       */
      state.budgets.push(action.payload.name);
      state.budgetsData.push({
        total: action.payload.amount,
        data: [],
      });
    },
    actionDeleteBudget: (state, action) => {
      /**
       * args : string (name)
       */
      let index = state.budgets.indexOf(action.payload);
      state.budgets.splice(index, 1);
      state.budgetsData.splice(index, 1);
    },
    actionAddAmount: (state, action) => {
      /**
       * args : {
       *      type : string (Income,Expens)
       *      amount : number
       *      label : string
       *      index : number
       *      date : string (YYYY-MM-DD)
       * }
       */
      state.budgetsData[action.payload.index].data.push({
        type: action.payload.type,
        amount: action.payload.amount,
        label: action.payload.label,
        date : action.payload.date
      });
      if (action.payload.type === "Income") {
        state.budgetsData[action.payload.index].total =
          parseFloat(state.budgetsData[action.payload.index].total) +
          parseFloat(action.payload.amount);
      } else if (action.payload.type === "Expens") {
        state.budgetsData[action.payload.index].total =
          parseFloat(state.budgetsData[action.payload.index].total) -
          parseFloat(action.payload.amount);
      }
    },
  },
});

export const { actionAddBudget, actionDeleteBudget, actionAddAmount } =
  budgetReducer.actions;
export default budgetReducer.reducer;
