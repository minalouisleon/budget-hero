import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    monthDistributionData : {}
}

const tempReducer = createSlice({
    name : 'tempReducer',
    initialState,
    reducers : {
        actionSetMonthDistributionData : (state,action) => {
            state.monthDistributionData = action.payload
        }
    }
})

export const {actionSetMonthDistributionData} = tempReducer.actions;
export default tempReducer.reducer;