// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBEan5cDYn-SFcjl_Z5mlSIrKxQ5piWfec",
  authDomain: "budget-hero-5f6d0.firebaseapp.com",
  projectId: "budget-hero-5f6d0",
  storageBucket: "budget-hero-5f6d0.appspot.com",
  messagingSenderId: "126838544589",
  appId: "1:126838544589:web:b5431affba257e437b3184",
  measurementId: "G-R32321DTRP"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth();
const analytics = getAnalytics(app);