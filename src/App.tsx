import { Redirect, Route } from "react-router-dom";
import { IonApp, IonRouterOutlet, setupIonicReact } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
// @ts-ignore
import Home from "./pages/Home/Home";
// @ts-ignore
import Login from "./pages/Login/Login";
// @ts-ignore
import AddBudget from "./pages/AddBudget/AddBudget";
// @ts-ignore
import BudgetPage from "./pages/BudgetPage/BudgetPage";
// @ts-ignore
import BudgetDetails from "./pages/BudgetDetails/BudgetDetails";
// @ts-ignore
import BudgetChart from "./pages/BudgetChart/BudgetChart";
// @ts-ignore
import MonthDistribution from './pages/MonthDistribution/MonthDistribution';
/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/home">
          <Home />
        </Route>
        <Route exact path="/">
          <Redirect to="/home" />
        </Route>
        <Route exact path="/Login">
          <Login />
        </Route>
        <Route exact path="/AddBudget">
          <AddBudget />
        </Route>
        <Route exact path="/BudgetPage/:name/:index">
          <BudgetPage />
        </Route>
        <Route exact path="/BudgetDetails/:name/:index">
          <BudgetDetails />
        </Route>
        <Route exact path="/BudgetChart/:name/:index/:totalBudget">
          <BudgetChart />
        </Route>
        <Route exact path="/MonthDistribution/:name/:index">
          <MonthDistribution />
        </Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
