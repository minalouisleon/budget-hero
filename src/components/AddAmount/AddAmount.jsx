import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React,{useState} from "react";
import { useDispatch } from "react-redux";
import { actionAddAmount } from "../../redux/budgetReducer";
import dayjs from "dayjs";
import {DatePicker} from 'antd';
const AddAmount = ({
  isAddAmountModalOpen,
  setIsAddAmountModalOpen,
  AddAmountType,
  setAddAmountType,
  index,
}) => {
  const dispatch = useDispatch(null);
  const [selectedDate, setSelectedDate] = useState(dayjs());
  const handleAddAmount = (e) => {
    e.preventDefault();
    let Date = "";
    if (e.target[2].value === undefined) {
      Date = dayjs(selectedDate).format("YYYY-MM-DD");
    } else {
      Date = dayjs(e.target[2].value).format("YYYY-MM-DD");
    }
    dispatch(
      actionAddAmount({
        type: AddAmountType,
        amount: e.target[1].value,
        label: e.target[0].value,
        index: index,
        date : Date
      })
    );
    setAddAmountType("");
    setIsAddAmountModalOpen(false);
  };
  return (
    <IonModal isOpen={isAddAmountModalOpen}>
      <IonHeader>
        <IonToolbar mode="ios">
          <IonTitle>Add {AddAmountType}</IonTitle>
          <IonButtons slot="end">
            <IonButton
              color="danger"
              onClick={() => setIsAddAmountModalOpen(false)}
            >
              Close
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <form onSubmit={handleAddAmount}>
          <IonList>
            <IonItem>
              <IonLabel>Label :</IonLabel>
              <IonInput type="text" />
            </IonItem>
            <IonItem>
              <IonLabel>Amount :</IonLabel>
              <IonInput type="number" step="0.01" required />
            </IonItem>
            <br/>
            <div className="tc">
              <DatePicker
                defaultValue={selectedDate}
                onChange={(date) => setSelectedDate(date)}
              />
            </div>
            <div className="tc">
              <IonButton color="success" type="submit">
                Add
              </IonButton>
            </div>
          </IonList>
        </form>
      </IonContent>
    </IonModal>
  );
};

export default AddAmount;
