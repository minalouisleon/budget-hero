import { IonButton } from "@ionic/react";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { auth } from "../../firebase/firebaseConfig";
import { signOut } from "firebase/auth";
import { useHistory } from "react-router";
import { actionLogoutUser } from "../../redux/userReducer";
const LoginLogoutBtn = () => {
  const dispatch = useDispatch(null);
  const history = useHistory(null);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const handleLogout = () => {
    signOut(auth)
      .then(dispatch(actionLogoutUser()), history.replace("/"))
      .catch((error) => console.log(error));
  };
  return (
    <>
      {isLoggedIn ? (
        <IonButton color="danger" onClick={() => handleLogout()}>
          Logout
        </IonButton>
      ) : (
        <IonButton onClick={() => history.push('/Login')}>Login</IonButton>
      )}
    </>
  );
};

export default LoginLogoutBtn;
